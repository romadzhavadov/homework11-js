const form = document.querySelector('form')
const icon = document.querySelectorAll('.fa-eye');
const inputs = document.querySelectorAll('.input-wrapper input')
const iconSlash = document.querySelectorAll('.fa-eye-slash');

const wrapper = document.querySelectorAll('.icon')



wrapper.forEach((elem) => {
    elem.addEventListener('click', (e) => {

        for (let icon of iconSlash) {
            if (icon.dataset.icon === e.target.dataset.icon) {
                icon.classList.toggle('active');
            } 
        }

        if (e.target.parentNode.parentNode.querySelector('input').getAttribute('type') === 'password') {
            e.target.parentNode.parentNode.querySelector('input').setAttribute('type', 'text');

        } else {
            e.target.parentNode.parentNode.querySelector('input').setAttribute('type', 'password');
        }

    })
})

form.addEventListener('submit', (e) => {
    e.preventDefault();

    if (inputs[0].value !== inputs[1].value) {

        const massage = document.getElementById('massage')

        const warn = document.createElement('p')
        warn.style.color = 'red';
        warn.innerText = `Потрібно ввести однакові значення`
        
        massage.append(warn)
    } else {
        alert(`You are welcome`)
    }

}, )